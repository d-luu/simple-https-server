To create a self signed cert with key:
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
  -subj '/CN=example.com' \  
  -keyout key.pem \
  -out cert.pem
```

To run:
```
docker run \
  -v $(pwd):/serve \
  -v {path_to_key}:/tls/key.pem \
  -v {path_to_cert}:/tls/cert.pem \
  -p 443:4443  
``` 
