FROM python:3.9.1-slim-buster

ADD simple-https-server.py /

RUN mkdir -p /tls /serve

ENTRYPOINT [ "/simple-https-server.py" ]

