#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
import os
import ssl

SERVE_DIR='/serve'
TLS_DIR='/tls'
SERVE_PORT=4443

os.chdir(SERVE_DIR)
httpd = HTTPServer(('localhost', SERVE_PORT), BaseHTTPRequestHandler)

httpd.socket = ssl.wrap_socket (httpd.socket, 
        keyfile='{}/key.pem'.format(TLS_DIR), 
        certfile='{}/cert.pem'.format(TLS_DIR), server_side=True)

httpd.serve_forever()
